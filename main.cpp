#include "jgame.h"
#include "jdraw8.h"
#include "jsound.h"
#include "jfile.h"
#include "info.h"
#include "modulegame.h"
#include "modulesequence.h"
#include "time.h"
#include <string>

/*
#ifndef WIN32
#include <libgen.h>
#endif
*/

int main( int argc, char* args[] ) {

	JF_SetResourceFile("data.jrf");
/*#ifdef WIN32
	JF_SetResourceFile("data.jrf");
#else
	char res_file[255] = "";
	strcpy(res_file, dirname(args[0]));
#ifdef __APPLE__
    strcat(res_file, "/../Resources/data.jrf");
#else
    strcat(res_file, "/data.jrf");
#endif
	printf("ARXIU DE RECURSOS: %s\n", res_file);
	JF_SetResourceFile(res_file);
#endif
*/
	srand( unsigned(time(NULL)) );

	JG_Init();
	JD8_Init("Aventures En Egipte");
	JS_Init();

	Info info;
	info.num_habitacio = 1;
	info.num_piramide = 255;
	info.diners = 0;
	info.diamants = 0;
	info.vida = 5;
	info.momies = 0;
	info.nou_personatge = false;
	info.pepe_activat = false;

	FILE* ini = fopen("trick.ini", "rb");
	if (ini != NULL) {
		info.nou_personatge = true;
		fclose(ini);
	}

	int gameState = 1;

	while (gameState != -1) {
		switch (gameState) {
			case 0:
				ModuleGame* moduleGame;
				moduleGame = new ModuleGame( &info );
				gameState = moduleGame->Go();
				delete moduleGame;
				break;
			case 1:
				ModuleSequence* moduleSequence;
				moduleSequence = new ModuleSequence( &info );
				gameState = moduleSequence->Go();
				delete moduleSequence;
				break;
		}
	}

	JF_Quit();
	JS_Finalize();
	JD8_Quit();
	JG_Finalize();

	return 0;
}

