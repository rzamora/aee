#include "sprite.h"

Sprite::Sprite( JD8_Surface gfx ) {

	this->gfx = gfx;
	this->entitat = NULL;

}

Sprite::~Sprite(void) {

	if( this->entitat != NULL ) {

		if( this->entitat->num_frames > 0 ) free( this->entitat->frames );

		if( this->entitat->num_animacions > 0 ) {
			for( int i = 0; i < this->entitat->num_animacions; i++ ) {
				if( this->entitat->animacions[i].num_frames > 0 ) free( this->entitat->animacions[i].frames );
			}
		}

		free( this->entitat );
	}

}

void Sprite::draw() {

	JD8_BlitCK( this->x, this->y, this->gfx, this->entitat->frames[this->entitat->animacions[this->o].frames[this->cur_frame]].x,
								   this->entitat->frames[this->entitat->animacions[this->o].frames[this->cur_frame]].y,
								   this->entitat->frames[this->entitat->animacions[this->o].frames[this->cur_frame]].w,
								   this->entitat->frames[this->entitat->animacions[this->o].frames[this->cur_frame]].h,
								   255);
	
}


