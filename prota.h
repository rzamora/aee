#pragma once

#include "sprite.h"
#include "info.h"

class Prota : public Sprite {

public:

	Prota( JD8_Surface gfx, Info* info );

	void draw();
	Uint8 update();

	Uint8 frame_pejades;
	bool pergami;

protected:

	Info* info;

};
