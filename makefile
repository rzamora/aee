TARGET=aee
all:
	g++ *.cpp -std=c++11 -lSDL2 -lSDL2_mixer -o $(TARGET)
	
debug:
	g++ *.cpp -g -std=c++11 -lSDL2 -lSDL2_mixer -o $(TARGET)
	
clean:
	rm -rf ./$(TARGET)

