#pragma once

#include "sprite.h"
#include "prota.h"
#include "info.h"

class Bola : public Sprite {

public:

	Bola( JD8_Surface gfx, Info* info, Prota* sam );

	void draw();
	void update();


protected:

	Uint8		contador;
	Prota*		sam;
	Info*		info;

};
