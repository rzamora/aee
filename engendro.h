#pragma once

#include "sprite.h"

class Engendro : public Sprite {

public:

	Engendro( JD8_Surface gfx, Uint16 x, Uint16 y );

	void draw();
	bool update();

protected:

	Uint8	vida;

};
