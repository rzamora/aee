#include "jdraw8.h"
#include "jfile.h"
#include <fstream>
#include "gif.h"

#ifdef GCWZERO
#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240
#else
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#endif

JD8_Surface screen = NULL;
JD8_Palette main_palette = NULL;
Uint32* pixel_data = NULL;

int screenWidth = 320;
int screenHeight = 200;

Uint32 contadorFPS = 0;
Uint32 tempsFPS = SDL_GetTicks();
char *fps = (char *)malloc(10);

SDL_Window* sdlWindow = NULL;
SDL_Renderer* sdlRenderer = NULL;
SDL_Texture* sdlTexture = NULL;

void JD8_Init(const char *title) {
	screen 			= (JD8_Surface)calloc( 1, 64000 );
	main_palette 	= (JD8_Palette)calloc( 1, 768 );
	pixel_data = (Uint32*)calloc(1, 320 * 200 * 4); // 1048576 );

	sdlWindow = SDL_CreateWindow( title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
	sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, 0);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");

	sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 320, 200);
}

void JD8_Quit() {
	if( screen != NULL ) free( screen );
	if( main_palette != NULL ) free( main_palette );
	if( pixel_data != NULL ) free( pixel_data );
	SDL_DestroyRenderer(sdlRenderer);
	SDL_DestroyWindow( sdlWindow );
}

void JD8_ClearScreen(Uint8 color) {
	memset( screen, color, 64000 );
}

JD8_Surface JD8_NewSurface() {
	JD8_Surface surface = (JD8_Surface)malloc( 64000 );
	memset( surface, 0, 64000 );
	return surface;
}

JD8_Surface JD8_LoadSurface(const char *file) {
	int filesize = 0;
	char *buffer = JF_GetBufferFromResource(file, filesize);
	
	unsigned short w, h;
	Uint8* pixels = LoadGif((unsigned char*)buffer, &w, &h);
	
	free(buffer);
	
	if (pixels == NULL) {
		printf("Unable to load bitmap: %s\n", SDL_GetError());
		exit(1);
	}
	
	JD8_Surface image = JD8_NewSurface();
	memcpy(image, pixels, 64000);

	free(pixels);
	return image;
}

JD8_Palette JD8_LoadPalette(const char *file) {
	int filesize = 0;
	char *buffer = NULL;
	buffer = JF_GetBufferFromResource(file, filesize);

	JD8_Palette palette = (JD8_Palette)LoadPalette((unsigned char*)buffer);

	return palette;
}

void JD8_SetScreenPalette(JD8_Palette palette) {
	if (main_palette == palette) return;
	if( main_palette != NULL) free( main_palette );
	main_palette = palette;
}

void JD8_FillSquare(int ini, int height, Uint8 color) {
	const int offset = ini * 320;
	const int size = height * 320;
	memset(&screen[offset], color, size);
}

void JD8_Blit(JD8_Surface surface) {
	memcpy( screen, surface, 64000 );
}

void JD8_Blit(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh) {
	int src_pointer = sx + (sy*320);
	int dst_pointer = x + (y*320);
	for( int i = 0; i < sh; i++ ) {
		memcpy( &screen[dst_pointer], &surface[src_pointer], sw );
		src_pointer += 320;
		dst_pointer += 320;
	}
}

void JD8_BlitToSurface(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, JD8_Surface dest) {
	int src_pointer = sx + (sy*320);
	int dst_pointer = x + (y*320);
	for( int i = 0; i < sh; i++ ) {
		memcpy( &dest[dst_pointer], &surface[src_pointer], sw );
		src_pointer += 320;
		dst_pointer += 320;
	}
}

void JD8_BlitCK(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, Uint8 colorkey ) {
	int src_pointer = sx + (sy*320);
	int dst_pointer = x + (y*320);
	for( int j = 0; j < sh; j++ ) {
		for( int i = 0; i < sw; i++ ) {
			if( surface[src_pointer+i] != colorkey ) screen[dst_pointer+i] = surface[src_pointer+i];
		}
		src_pointer += 320;
		dst_pointer += 320;
	}
}

void JD8_BlitCKCut(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, Uint8 colorkey) {
	int src_pointer = sx + (sy * 320);
	int dst_pointer = x + (y * 320);
	for (int j = 0; j < sh; j++) {
		for (int i = 0; i < sw; i++) {
			if (surface[src_pointer + i] != colorkey && (x+i >= 0) && (y+j >= 0) && (x+i < 320) && (y+j < 200)) screen[dst_pointer + i] = surface[src_pointer + i];
		}
		src_pointer += 320;
		dst_pointer += 320;
	}
}

void JD8_BlitCKScroll(int y, JD8_Surface surface, int sx, int sy, int sh, Uint8 colorkey) {
	int dst_pointer = y * 320;
	for (int j = sy; j < sy+sh; j++) {
		for (int i = 0; i < 320; i++) {
			int x = (i+sx) % 320;
			if (surface[x + j*320] != colorkey) screen[dst_pointer] = surface[x + j * 320];
			dst_pointer++;
		}
	}
}

void JD8_BlitCKToSurface(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, JD8_Surface dest, Uint8 colorkey) {
	int src_pointer = sx + (sy*320);
	int dst_pointer = x + (y*320);
	for( int j = 0; j < sh; j++ ) {
		for( int i = 0; i < sw; i++ ) {
			if( surface[src_pointer+i] != colorkey ) dest[dst_pointer+i] = surface[src_pointer+i];
		}
		src_pointer += 320;
		dst_pointer += 320;
	}
}

SDL_Rect rect{0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

void JD8_Flip() {
	for( int x = 0; x < 320; x++ ) {
		for( int y = 0; y < 200; y++ ) {
			Uint32 color = 0xFF000000 + main_palette[screen[x + ( y * 320 )]].r + ( main_palette[screen[x + ( y * 320 )]].g << 8 ) + ( main_palette[screen[x + ( y * 320 )]].b << 16 );
			pixel_data[x + ( y * 320 )] = color;
		}
	}
	SDL_UpdateTexture(sdlTexture, NULL, pixel_data, 320 * sizeof(Uint32));
	SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &rect);
	SDL_RenderPresent(sdlRenderer);
}

void JD8_FreeSurface(JD8_Surface surface) {
	free( surface );
}

Uint8 JD8_GetPixel( JD8_Surface surface, int x, int y ) {
	return surface[x + (y*320)];
}

void JD8_PutPixel( JD8_Surface surface, int x, int y, Uint8 pixel ) {
	surface[x + (y*320)] = pixel;
}

void JD8_SetPaletteColor(Uint8 index, Uint8 r, Uint8 g, Uint8 b) {
	main_palette[index].r = r << 2;
	main_palette[index].g = g << 2;
	main_palette[index].b = b << 2;
}

void JD8_FadeOut() {
	for( int j = 0; j < 32; j++ ) {
		for( int i = 0; i < 256; i++ ) {
			if( main_palette[i].r >= 8 ) main_palette[i].r-=8; else main_palette[i].r=0;
			if( main_palette[i].g >= 8 ) main_palette[i].g-=8; else main_palette[i].g=0;
			if( main_palette[i].b >= 8 ) main_palette[i].b-=8; else main_palette[i].b=0;
		}
		JD8_Flip();
	}
}

#define MAX(a, b) (a) > (b) ? (a) : (b)

void JD8_FadeToPal( JD8_Palette pal ) {
	for( int j = 0; j < 32; j++ ) {
		for( int i = 0; i < 256; i++ ) {
			if( main_palette[i].r <= int(pal[i].r)-8 ) main_palette[i].r+=8; else main_palette[i].r=pal[i].r;
			if( main_palette[i].g <= int(pal[i].g)-8 ) main_palette[i].g+=8; else main_palette[i].g=pal[i].g;
			if( main_palette[i].b <= int(pal[i].b)-8 ) main_palette[i].b+=8; else main_palette[i].b=pal[i].b;
		}
		JD8_Flip();
	}
}
