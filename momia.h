#pragma once

#include "sprite.h"
#include "prota.h"
#include "engendro.h"
#include "info.h"

class Momia : public Sprite {

public:

	Momia( JD8_Surface gfx, Info* info, bool dimoni, Uint16 x, Uint16 y, Prota* sam );

	void clear();
	void draw();
	bool update();
	void insertar( Momia* momia );

	bool dimoni;
	Momia*	next;

protected:

	Prota*		sam;
	Info*		info;
	Engendro*	engendro;

};
