#pragma once

#include "jdraw8.h"

#include "info.h"
#include "prota.h"

#define CONTE_RES 0
#define CONTE_TRESOR 1
#define CONTE_FARAO 2
#define CONTE_CLAU 3
#define CONTE_MOMIA 4
#define CONTE_PERGAMI 5
#define CONTE_DIAMANT 6
#define VALOR_DIAMANT 5

struct Tomba {
	bool	costat[4];
	Uint8	contingut;
	bool	oberta;
	Uint16	x, y;
};

struct Vertex {
	Uint8	columna;
	Uint8	fila;
};

class Mapa {

public:

	Mapa( JD8_Surface gfx, Info* info, Prota* sam );
	~Mapa(void);

	void draw();
	void update();
	bool novaMomia();
	void comprovaCaixa( Uint8 num );

	Tomba		tombes[16];

protected:

	void preparaFondoEstatic();
	void preparaTombes();

	void comprovaUltimCami();
	void comprovaPorta();

	JD8_Surface	gfx;
	JD8_Surface fondo;
	Vertex		vertex;
	Vertex		ultim_vertex;
	Info*		info;
	Uint8		frame_torxes;

	Prota*		sam;

	bool		farao;
	bool		clau;
	bool		porta_oberta;
	bool		nova_momia;

};
