#include "modulesequence.h"

#include "jgame.h"
#include "jdraw8.h"
#include "jinput.h"
#include "jsound.h"
#include <stdlib.h>
#include <string>

ModuleSequence::ModuleSequence( Info* info ) {
	this->info = info;
}

ModuleSequence::~ModuleSequence(void) {
}

int ModuleSequence::Go() {

	if( this->info->num_piramide == 6 && this->info->diners < 200 ) this->info->num_piramide = 7;

	switch( this->info->num_piramide ) {
		case 255:	// Intro
			doIntro();
			break;
		case 0:		// Men�
			doMenu();
			break;
		case 1:		// Slides
		case 7:
			doSlides();
			break;
		case 2:		// Pre-pir�mide
		case 3:
		case 4:
		case 5:
			doBanner();
			break;
		case 6:		// Pre-Secreta
			doSecreta();
			break;
		case 8:		// Credits
			doCredits();
			break;
        case 100:	// Mort
            doMort();
            break;
	}

	JD8_FadeOut();

	if( JG_Quitting() ) {
		return -1;
	} else {
		if( this->info->num_piramide == 255 ) {
			this->info->num_piramide = 0;
			return 1;
		} else if( this->info->num_piramide == 0 ) {
			this->info->num_piramide = 1;
			//this->info->num_piramide = 6;
			//this->info->diners = 200;
			return 1;
		} else if( this->info->num_piramide == 7 ) {
			this->info->num_piramide = 8;
			return 1;
		} else if( this->info->num_piramide == 8 ) {
			this->info->num_piramide = 255;
            return 1;
        } else if( this->info->num_piramide == 100 ) {
            this->info->num_piramide = 0;
            return 1;
		} else {
			return 0;
		}

	}
    return 0;
}

const int minim( const int a, const int b ) {
	if( b < a ) { return b; } else { return a; }
}

void ModuleSequence::doIntro() {
	JG_SetUpdateTicks(1000);

	JS_LoadMusic("00000003.xm");
	JS_PlayMusic(-1);

	JD8_Surface gfx = JD8_LoadSurface( "logo.gif" );
	JD8_Palette pal = JD8_LoadPalette( "logo.gif" );
	JD8_SetScreenPalette( pal );

	JD8_ClearScreen( 0 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JG_SetUpdateTicks(100);

	JD8_Blit( 43, 78, gfx, 43, 155, 27, 45 );
	JD8_Blit( 68, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_Blit( 43, 78, gfx, 43, 155, 53, 45 );
	JD8_Blit( 96, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_Blit( 43, 78, gfx, 43, 155, 66, 45 );
	JD8_Blit( 109, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JG_SetUpdateTicks(200);

	JD8_Blit( 43, 78, gfx, 43, 155, 92, 45 );
	JD8_Blit( 136, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_ClearScreen( 0 );
	JD8_Blit( 43, 78, gfx, 43, 155, 92, 45 );
	//JD8_Blit( 136, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JG_SetUpdateTicks(100);

	JD8_Blit( 43, 78, gfx, 43, 155, 118, 45 );
	JD8_Blit( 160, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_Blit( 43, 78, gfx, 43, 155, 145, 45 );
	JD8_Blit( 188, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_Blit( 43, 78, gfx, 43, 155, 178, 45 );
	JD8_Blit( 221, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JD8_Blit( 43, 78, gfx, 43, 155, 205, 45 );
	JD8_Blit( 248, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	JG_SetUpdateTicks(200);
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Blit( 274, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JD8_ClearScreen( 0 );
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Blit( 274, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JD8_ClearScreen( 0 );
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Blit( 274, 78, gfx, 274, 155, 27, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JD8_ClearScreen( 0 );
	JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
	JD8_Flip();
	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }

	for( int j = 0; j < 256; j++ ) {
		for( int i = 16; i < 32; i++ ) {
			if( i == 17 ) {
				if( pal[i].r < 255 ) pal[i].r++;
				if( pal[i].g < 255 ) pal[i].g++;
				if( pal[i].b < 255 ) pal[i].b++;
			}
			if( pal[i].b < pal[i].g ) pal[i].b++;
			if( pal[i].b > pal[i].g ) pal[i].b--;
			if( pal[i].r < pal[i].g ) pal[i].r++;
			if( pal[i].r > pal[i].g ) pal[i].r--;
		}
		JD8_Flip();
	}

	while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
	JG_SetUpdateTicks(20);

	Uint16 fr1 = 13;
	Uint16 fr2 = fr1;
 	Uint16 fr3 = 11;
 	Uint16 fr4 = fr3;
 	Uint16 fr5 = 20;
 	Uint16 fr6 = 8;
 	Uint16 fr7 = 29;
	Uint16 fr8 = 4;
	Uint16 fr9 = 16;
	Uint16 fr10 = fr9;
	Uint16 fr11 = 6;
	Uint16 creu = 75;
	Uint16 interrogant = 90;

	Uint16 fr_ani_1[13]; // camina dreta
	Uint16 fr_ani_2[13]; // camina esquerra
	Uint16 fr_ani_3[11]; // trau el mapa DRETA
	Uint16 fr_ani_4[11]; // trau el mapa ESQUERRA
	Uint16 fr_ani_5[20]; // bot de susto
	Uint16 fr_ani_6[8]; // momia
	Uint16 fr_ani_7[29]; // deixa caure el PAPER i SOMBRA
	Uint16 fr_ani_8[4];  // PEDRA
	Uint16 fr_ani_9[16];  // prota BALL
	Uint16 fr_ani_10[16]; // momia BALL
	Uint16 fr_ani_11[6]; // altaveu

	for( int i=0; i < fr1; i++ ) fr_ani_1[i] = i*15;
	for( int i=0; i < fr2; i++ ) fr_ani_2[i] = i*15; //15
	for( int i=0; i < fr3; i++ ) fr_ani_3[i] = i*15; //30
	for( int i=0; i < fr4; i++ ) fr_ani_4[i] = i*15; //45
	for( int i=0; i <= 9; i++ ) fr_ani_5[i] = (i+11)*15; //45
	for( int i=10; i <= 19; i++ ) fr_ani_5[i] = fr_ani_5[19-i];
	for( int i=0; i < fr6; i++ ) fr_ani_6[i] = i*15; //60
	for( int i=0; i <= 13; i++ ) fr_ani_7[i] = (i+5)*15; //75
	for( int i=14; i < fr7; i++ ) fr_ani_7[i] = (i-14)*15; //105
	for( int i=0; i < fr8; i++ ) fr_ani_8[i] = (i+1)*15; //75
	for( int i=0; i < fr9; i++ ) fr_ani_9[i] = i*15; //120
	for( int i=0; i < fr10; i++ ) fr_ani_10[i] = i*15; //135
	for( int i=0; i < fr11; i++ ) fr_ani_11[i] = (i+1)*15; //90
	fr_ani_11[fr11-1] = fr_ani_11[3];

	switch( rand()%3 ) {
		case 0:

			// camina cap a la DRETA }
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_1[(i div 5) mod fr1],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_1[(i / 5) % fr1], 0, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // trau el MAPA DRETA }
			 
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_3[minim((i div 5),fr3-1)],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_3[minim((i / 5), fr3-1)], 30, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // guarda el MAPA }
			 
			 for( int i = 200; i >= 0; i-- ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_3[minim((i div 5),fr3-1)],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_3[minim((i / 5), fr3-1)], 30, 15, 15, 0 );

			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // camina cap a la ESQUERRA }
			 
			 for( int i = 200; i >= 80; i-- ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_2[(i div 5) mod fr2],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_2[(i / 5) % fr2], 15, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // trau el MAPA ESQUERRA }
			 
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_4[minim((i div 5),fr4-1)],15,15,80,150);
			   JD8_BlitCK( 80, 150, gfx, fr_ani_4[minim((i / 5), fr4-1)], 45, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // momia cap a la ESQUERRA }
			 
			 for( int i = 300; i >= 95; i-- ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_6[(i div 10) mod fr6],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_6[(i / 5) % fr6], 60, 15, 15, 0 );
			   //Put_sprite(from,where,fr_ani_4[fr4-1],15,15,80,150);
			   JD8_BlitCK( 80, 150, gfx, fr_ani_4[fr4-1], 45, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // girar-se }
			 
			 for( int i = 0; i <= 50; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_1[1],15,15,80,150);
			   JD8_BlitCK( 80, 150, gfx, fr_ani_1[1], 0, 15, 15, 0 );
			   //Put_sprite(from,where,fr_ani_6[4],15,15,95,150);
			   JD8_BlitCK( 95, 150, gfx, fr_ani_6[4], 60, 15, 15, 0 );
			   //Put_sprite(from,where,interrogant,15,15,80,133);
			   JD8_BlitCK( 80, 133, gfx, 0, interrogant, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // bot de SUSTO }
			 
			 for( int i = 0; i <= 49; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_5[minim((i div 5),fr5-1)],15,15,80,150-((i mod 50) div 5));
			   JD8_BlitCK( 80, 150-((i % 50) / 5), gfx, fr_ani_5[minim(i/5, fr5-1)], 45, 15, 15, 0 );
			   //Put_sprite(from,where,fr_ani_6[4],15,15,95,150);
			   JD8_BlitCK( 95, 150, gfx, fr_ani_6[4], 60, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // bot de SUSTO }
			 
			 for( int i = 50; i <= 99; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_5[minim((i div 5),fr5-1)],15,15,80,140+((i mod 50) div 5));
			   JD8_BlitCK( 80, 140+((i % 50) / 5), gfx, fr_ani_5[minim(i/5, fr5-1)], 45, 15, 15, 0 );
			   //Put_sprite(from,where,fr_ani_6[4],15,15,95,150);
			   JD8_BlitCK( 95, 150, gfx, fr_ani_6[4], 60, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // camina cap a la ESQUERRA }
			 
			 for( int i = 80; i >= 0; i-- ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_2[(i div 5) mod fr2],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_2[(i/5) % fr2], 15, 15, 15, 0 );
			   //Put_sprite(from,where,fr_ani_6[4],15,15,95,150);
			   JD8_BlitCK( 95, 150, gfx, fr_ani_6[4], 60, 15, 15, 0 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // final }
			 
			 for( int i = 0; i <= 150; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_6[4],15,15,95,150);
			   JD8_BlitCK( 95, 150, gfx, fr_ani_6[4], 60, 15, 15, 0 );
			   //Put_sprite(from,where,interrogant,15,15,95,133);
			   JD8_BlitCK( 95, 133, gfx, 0, interrogant, 15, 15, 0 );
			   
			   JD8_Flip();
			  }
			 //-----}


			break;
		case 1:
			 // camina cap a la DRETA }
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_1[(i div 5) mod fr1],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_1[(i/5) % fr1], 0, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // trau el MAPA DRETA }
			 
			 for( int i = 0; i <= 300; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_3[minim((i div 5),fr3-1)],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_3[minim(i/5, fr3-1)], 30, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // INTERROGANT }
			 
			 for( int i = 0; i <= 100; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,interrogant,15,15,200,134);
			   JD8_BlitCK( 200, 134, gfx, 0, interrogant, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_3[fr3-1],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_3[fr3-1], 30, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // deixa caure el MAPA i SOMBRA }
			 
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_7[minim((i div 5),fr7-1)],15,15,200,150);
			   if( minim(i/5, fr7-1) <= 13 ) {
					JD8_BlitCK( 200, 150, gfx, fr_ani_7[minim(i/5, fr7-1)], 75, 15, 15, 255 );
			   } else {
					JD8_BlitCK( 200, 150, gfx, fr_ani_7[minim(i/5, fr7-1)], 105, 15, 15, 255 );
			   }
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // SOMBRA i PEDRA }
			 
			 for( int i = 0; i <= 75; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_7[fr7-1],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_7[fr7-1], 105, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_8[0],15,15,200,i*2);
			   JD8_BlitCK( 200, i*2, gfx, fr_ani_8[0], 75, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // trencar PEDRA }
			 
			 for( int i = 0; i <= 19; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_8[i div 10],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_8[i/10], 75, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // FINAL }
			 
			 for( int i = 0; i <= 200; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,creu,15,15,200,155);
			   JD8_BlitCK( 200, 155, gfx, 0, creu, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_8[1],15,15,200,150);
			   JD8_BlitCK( 200, 150, gfx, fr_ani_8[1], 75, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_8[2],15,15,185,150);
			   JD8_BlitCK( 185, 150, gfx, fr_ani_8[2], 75, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_8[3],15,15,215,150);
			   JD8_BlitCK( 215, 150, gfx, fr_ani_8[3], 75, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }
			break;
		case 2:
			 // camina cap a la DRETA }
			 for( int i = 0; i <= 145; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_1[(i div 5) mod fr1],15,15,i,150);
			   JD8_BlitCK( i, 150, gfx, fr_ani_1[(i/5)  % fr1], 0, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_6[(i div 10) mod fr6],15,15,304-i,150);
			   JD8_BlitCK( 304-i, 150, gfx, fr_ani_6[(i/10) % fr6], 60, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // els dos quets }
			 
			 for( int i = 0; i <= 100; i++ ) {

				 JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_1[1],15,15,145,150);
			   JD8_BlitCK( 145, 150, gfx, fr_ani_1[1], 0, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_6[1],15,15,160,150);
			   JD8_BlitCK( 160, 150, gfx, fr_ani_6[1], 60, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // aparicio altaveu }
			 
			 for( int i = 0; i <= 50; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_11[(i div 10) mod 2],15,15,125,150);
			   JD8_BlitCK( 125, 150, gfx, fr_ani_11[(i/10) % 2], 90, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_1[1],15,15,145,150);
			   JD8_BlitCK( 145, 150, gfx, fr_ani_1[1], 0, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_6[1],15,15,160,150);
			   JD8_BlitCK( 160, 150, gfx, fr_ani_6[1], 60, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			 // BALL }
			 
			 for( int i = 0; i <= 800; i++ ) {
			  
			   JD8_ClearScreen( 0 );
			   JD8_Blit( 43, 78, gfx, 43, 155, 231, 45 );
			   //Put_sprite(from,where,fr_ani_9[(i div 10) mod fr9],15,15,145,150);
			   JD8_BlitCK( 145, 150, gfx, fr_ani_9[(i/10) % fr9], 120, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_10[(i div 10) mod fr10],15,15,160,150);
			   JD8_BlitCK( 160, 150, gfx, fr_ani_10[(i/10) % fr10], 135, 15, 15, 255 );
			   //Put_sprite(from,where,fr_ani_11[((i div 5) mod 4)+2],15,15,125,150);
			   JD8_BlitCK( 125, 150, gfx, fr_ani_11[((i/5) % 4)+2], 90, 15, 15, 255 );
			   
			   JD8_Flip();
			   while( !JG_ShouldUpdate() ) { JI_Update(); if( JI_AnyKey() || JG_Quitting() ) { JD8_FreeSurface( gfx ); return; } }
			  }

			break;
	}


	JD8_FreeSurface( gfx );
}


void ModuleSequence::doMenu() {
	JG_SetUpdateTicks(20);
	JD8_Surface fondo = JD8_LoadSurface( "menu.gif" );
	JD8_Surface gfx = JD8_LoadSurface( "menu2.gif" );
	JD8_Palette pal = JD8_LoadPalette( "menu2.gif" );
	
	JD8_Blit( fondo );
	JD8_BlitCK( 100, 25, gfx, 0, 74, 124, 68, 255 ); // logo
	JD8_BlitCK( 130, 100, gfx, 0, 0, 80, 74, 255 );
	JD8_BlitCK( 0, 150, gfx, 0, 150, 320, 50, 255 );
	JD8_FadeToPal( pal );

	contador = 0;
	int palmeres = 0;
	int horitzo = 0;
	int camello = 0;

	JI_Update();
	while( !JI_AnyKey() && !JG_Quitting() && !JI_KeyPressed(SDL_SCANCODE_P)) {

		JD8_Blit( 0, 0, fondo, 0, 0, 320, 100 ); // fondo sol estatic

		JD8_BlitCK( horitzo, 100, fondo, 0, 100, 320-horitzo, 100, 255 ); // fondo moviment
		JD8_BlitCK( 0, 100, fondo, 320-horitzo, 100, horitzo, 100, 255 );

		JD8_BlitCK( 100, 25, gfx, 0, 74, 124, 68, 255 ); // logo
		JD8_BlitCK( 130, 100, gfx, camello*80, 0, 80, 74, 255 ); // camello

		JD8_BlitCK( palmeres, 150, gfx, 0, 150, 320-palmeres, 50, 255 ); // palemeres moviment
		JD8_BlitCK( 0, 150, gfx, 320-palmeres, 150, palmeres, 50, 255 );

		JD8_BlitCK( 87, 167, gfx, 127, 124, 150, 24, 255 ); // jdes
		JD8_BlitCK( 303, 193, gfx, 305, 143, 15, 5, 255 ); // versio

		if( contador%100 > 30 ) JD8_BlitCK( 98, 130, gfx, 161, 92, 127, 9, 255 ); // pulsa tecla...
		if ((contador % 100 > 30) && this->info->nou_personatge) JD8_BlitCK(68, 141, gfx, 128, 105, 189, 9, 255); // 'p' per a personatge nou...

		JD8_Flip();

		if( JG_ShouldUpdate() ) {
			if( contador%4 == 0 ) { palmeres--; if( palmeres < 0 ) palmeres = 319; }
			if( contador%8 == 0 ) { camello++; if( camello == 4 ) camello = 0; }
			if( contador%16 == 0 ) { horitzo--; if( horitzo < 0 ) horitzo = 319; }
			contador++;	
			JI_Update();
		}
	}
	this->info->pepe_activat = JI_KeyPressed(SDL_SCANCODE_P);
	JI_DisableKeyboard(60);
	JD8_FreeSurface( fondo );
	JD8_FreeSurface( gfx );
	free( pal );
}

void ModuleSequence::doSlides() {
	JG_SetUpdateTicks(20);


	const char* arxiu;
	if( this->info->num_piramide == 7 ) {
		JS_LoadMusic("00000005.xm");
		JS_PlayMusic(1);
		if (this->info->diners < 200) {
			arxiu = "intro2.gif";
		} else {
			arxiu = "intro3.gif";
		}
	} else {
		arxiu = "intro.gif";
	}

	JD8_Surface gfx = JD8_LoadSurface( arxiu );
	JD8_Palette pal_aux = JD8_LoadPalette( arxiu );
	JD8_Palette pal = (JD8_Palette)malloc( 768 );
	memcpy( pal, pal_aux, 768 );
	JD8_ClearScreen( 255 );
	JD8_SetScreenPalette( pal );

	bool exit = false;
	int step = 0;
	contador = 1;
	while( !exit && !JG_Quitting() ) {
		if (JG_ShouldUpdate()) {
			JI_Update();

			if( JI_AnyKey() ) {
				exit = true;
			}

			switch( step ) {
				case 0:
					JD8_Blit( 320 - ( contador * 4 ), 65, gfx, 0, 0, contador*4, 65 );
					JD8_Flip();
					contador++;
					if( contador == 80 ) step++;
					break;
				case 3:
					JD8_Blit( 0, 65, gfx, 320 - ( contador * 4 ), 65, contador*4, 65 );
					JD8_Flip();
					contador++;
					if( contador == 80 ) step++;
					break;
				case 6:
					JD8_Blit( 320 - ( contador * 4 ), 65, gfx, 0, 130, contador*4, 65 );
					JD8_Flip();
					contador++;
					if( contador == 80 ) step++;
					break;
				case 1:
				case 4:
				case 7:
					contador--;
					if (contador == -150) { contador = 0;  step++; }
					break;
				case 2:
				case 5:
					JD8_FadeOut();
					memcpy( pal, pal_aux, 768 );
					JD8_ClearScreen( 255 );
					step++;
					break;
				case 8:
					if (this->info->num_piramide != 7) JS_FadeOutMusic();
					exit = true;
					break;
			}
		}
	}

	JD8_FreeSurface( gfx );
	free( pal_aux );
}

void ModuleSequence::doBanner() {
    JS_LoadMusic("00000004.xm");
    JS_PlayMusic(-1);

	this->contador = 5000;

	JD8_Surface gfx = JD8_LoadSurface( "ffase.gif" );
	JD8_Palette pal = JD8_LoadPalette( "ffase.gif" );

	JD8_ClearScreen( 0 );

	JD8_Blit( 81, 24, gfx, 81, 155, 168, 21 );
	JD8_Blit( 39, 150, gfx, 39, 175, 248, 20 );

	switch( this->info->num_piramide ) {
		case 2:
			JD8_Blit( 82, 60, gfx, 0, 0, 160, 75 );
			break;
		case 3:
			JD8_Blit( 82, 60, gfx, 160, 0, 160, 75 );
			break;
		case 4:
			JD8_Blit( 82, 60, gfx, 0, 75, 160, 75 );
			break;
		case 5:
			JD8_Blit( 82, 60, gfx, 160, 75, 160, 75 );
			break;
	}
	JD8_FadeToPal( pal );

	bool exit = false;
	while( !exit && !JG_Quitting() ) {
		if (JG_ShouldUpdate()) {
			JI_Update();

			if( JI_AnyKey() ) {
				exit = true;
			}

			contador--;
			if( contador == 0 ) exit = true;
		}
	}
    JS_FadeOutMusic();
}

void ModuleSequence::doSecreta() {
	JS_LoadMusic("00000002.xm");
	JS_PlayMusic(-1);
	JG_SetUpdateTicks(20);
	JD8_FadeOut();
	JD8_Surface gfx = JD8_LoadSurface("tomba1.gif");
	JD8_Palette pal_aux = JD8_LoadPalette("tomba1.gif");
	JD8_Palette pal = (JD8_Palette)malloc(768);
	memcpy(pal, pal_aux, 768);
	JD8_ClearScreen(255);
	JD8_SetScreenPalette(pal);

	bool exit = false;
	int step = 0;
	contador = 1;
	while (!exit && !JG_Quitting()) {
		if (JG_ShouldUpdate()) {
			JI_Update();

			if (JI_AnyKey()) {
				exit = true;
			}

			switch (step) {
			case 0:
				JD8_Blit(70, 60, gfx, 0, contador, 178, 70); //Put_Sprite(from, where, 0 + (320 * i), 178, 70, 70, 60);
				JD8_BlitCK(70, 60, gfx, 178, contador >> 1, 142, 70, 255); //Put_Sprite(from, where, 178 + (320 * (i div 2)), 142, 70, 70, 60);
				JD8_Flip();
				contador++;
				if (contador == 128) step++;
				break;
			case 1:
			case 4:
			case 7:
			case 9:
				contador--;
				if (contador == 0) step++;
				break;
			case 2:
				JD8_ClearScreen(255);
				JD8_Flip();
				gfx = JD8_LoadSurface("tomba2.gif");
				pal_aux = JD8_LoadPalette("tomba2.gif");
				memcpy(pal, pal_aux, 768);
				JD8_SetScreenPalette(pal);
				step++;
				break;
			case 3:
				JD8_Blit(55, 53, gfx, 0, 158-contador, 211, contador); //Put_Sprite(from, where, 0 + ((158 - i) * 320), 211, i, 55, 53);
				JD8_Flip();
				contador++;
				if (contador == 94) step++;
				break;
			case 5:
				JD8_ClearScreen(0);
				JD8_Flip();
				JD8_SetPaletteColor(254, 12, 11, 11);
				JD8_SetPaletteColor(253, 12, 11, 11);
				step++;
				break;
			case 6:
				JD8_Blit(80, 68, gfx, 160 - (contador*2), 0, contador*2, 64); //Put_Sprite(from, where, 160 - (i * 2), (i * 2), 64, 80, 68);
				JD8_Flip();
				contador++;
				if (contador == 80) step++;
				break;
			case 8:
				JD8_SetPaletteColor(254, contador+12, 11, 11);
				JD8_SetPaletteColor(253, (contador+12) >> 1, 11, 11);
				JD8_Flip();
				contador++;
				if (contador == 51) step++;
				break;
			case 10:
				JD8_FadeOut();
				memcpy(pal, pal_aux, 768);
				JD8_ClearScreen(255);
				JS_FadeOutMusic();
				exit = true;
				break;
			}
		}
	}

	JD8_FreeSurface(gfx);
	free(pal_aux);
}

void ModuleSequence::doCredits() {

	struct { Uint16 x, y; } frames_coche[8] = { { 214, 152 }, { 214, 104 }, { 214, 56 }, { 214, 104 }, { 214, 152 }, { 214, 8 }, { 108, 152 }, { 214, 8 } };
	const Uint32 n_frames_coche = 8;
	const Uint32 velocitat_coche = 3;

	JD8_Surface vaddr2 = JD8_LoadSurface("final.gif");
	JD8_Surface vaddr3 = JD8_LoadSurface("finals.gif");
	JD8_Palette pal = JD8_LoadPalette("final.gif");
	JD8_SetScreenPalette(pal);

	int contador = 0;

	bool exit = false;
	int step = 0;
	contador = 1;
	while (!exit && !JG_Quitting()) {
		if (JG_ShouldUpdate()) {
			JI_Update();

			if (JI_AnyKey() || contador >= 3100) {
				exit = true;
			}

			JD8_ClearScreen(255);

			if (contador < 2750) {
				JD8_BlitCKCut(115, 200 - (contador / 6), vaddr2, 0, 0, 80, 200, 0);
			}

			if ((contador > 1200) && (contador < 2280)) {
				JD8_BlitCKCut(100, 200 - ((contador-1200) / 6), vaddr2, 85, 0, 120, 140, 0);
			} else if (contador >= 2250) {
				JD8_BlitCK(100, 20, vaddr2, 85, 0, 120, 140, 0);
			}

			if (this->info->diamants == 16) {
				//scroll_final_joc(vaddr3, vaddr, contador_final);
				JD8_BlitCKScroll(50, vaddr3, ((contador >> 3) % 320) + 1, 0, 50, 255);
				JD8_BlitCKScroll(50, vaddr3, ((contador >> 2) % 320) + 1, 50, 50, 255);
				JD8_BlitCKScroll(50, vaddr3, ((contador >> 1) % 320) + 1, 100, 50, 255);
				JD8_BlitCKScroll(50, vaddr3, (contador % 320) + 1, 150, 50, 255);

				JD8_BlitCK(100, 50, vaddr2, frames_coche[(contador / velocitat_coche) % n_frames_coche].x, frames_coche[(contador / velocitat_coche) % n_frames_coche].y, 106, 48, 255);
			} else {
				JD8_BlitCK(0, 50, vaddr3, 0, 0, 320, 50, 255);
				JD8_BlitCK(0, 50, vaddr3, 0, 50, 320, 50, 255);
			}

			JD8_FillSquare(0, 50, 255);
			JD8_FillSquare(100, 10, 255);

			JD8_Flip();
			contador++;
		}
	}

	FILE* ini = fopen("trick.ini", "wb");
	fwrite("1", 1, 1, ini);
	fclose(ini);
	this->info->nou_personatge = true;

	JD8_FreeSurface(vaddr3);
	JD8_FreeSurface(vaddr2);
}

void ModuleSequence::doMort() {
    JS_LoadMusic("00000001.xm");
    JS_PlayMusic(1);

	JI_DisableKeyboard(60);

	this->info->vida = 5;
    this->contador = 1000;

    JD8_Surface gfx = JD8_LoadSurface( "gameover.gif" );
    JD8_Palette pal = JD8_LoadPalette( "gameover.gif" );

    JD8_ClearScreen( 0 );

    JD8_Blit( gfx );

    JD8_FadeToPal( pal );

    bool exit = false;
    while( !exit && !JG_Quitting() ) {
        if (JG_ShouldUpdate()) {
            JI_Update();
            
            if( JI_AnyKey() ) {
                exit = true;
            }
            
            contador--;
            if( contador == 0 ) exit = true;
        }
    }
	JS_LoadMusic("00000003.xm");
	JS_PlayMusic(-1);
}

