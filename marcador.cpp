#include "marcador.h"

Marcador::Marcador( JD8_Surface gfx, Info* info, Prota* sam ) {

	this->gfx = gfx;
	this->info = info;
	this->sam = sam;

}

Marcador::~Marcador(void) {	
}

void Marcador::draw() {
	
	if (this->info->num_piramide < 6) {
		this->pintaNumero(55, 2, this->info->num_piramide);
		this->pintaNumero(80, 2, this->info->num_habitacio);
	}

	this->pintaNumero( 149, 2, this->info->diners / 100 );
	this->pintaNumero( 156, 2, ( this->info->diners % 100 ) / 10 );
	this->pintaNumero( 163, 2, this->info->diners % 10 );

	if( this->sam->pergami ) JD8_BlitCK( 190, 1, this->gfx, 209, 185, 15, 14, 255 );

	JD8_BlitCK( 271, 1, this->gfx, 0, 20, 15, this->info->vida*3, 255 );
	if( this->info->vida < 5 ) JD8_BlitCK( 271, 1+(this->info->vida*3), this->gfx, 75, 20, 15, 15-(this->info->vida*3), 255 );
}

void Marcador::pintaNumero( Uint16 x, Uint16 y, Uint8 num ) {
	switch( num ) {
		case 0:
			JD8_BlitCK( x, y, this->gfx, 141, 193, 10, 7, 255 );
			break;
		case 1:
			JD8_BlitCK( x, y, this->gfx, 100, 185, 10, 7, 255 );
			break;
		case 2:
			JD8_BlitCK( x, y, this->gfx, 110, 185, 10, 7, 255 );
			break;
		case 3:
			JD8_BlitCK( x, y, this->gfx, 120, 185, 10, 7, 255 );
			break;
		case 4:
			JD8_BlitCK( x, y, this->gfx, 130, 185, 10, 7, 255 );
			break;
		case 5:
			JD8_BlitCK( x, y, this->gfx, 140, 185, 10, 7, 255 );
			break;
		case 6:
			JD8_BlitCK( x, y, this->gfx, 101, 193, 10, 7, 255 );
			break;
		case 7:
			JD8_BlitCK( x, y, this->gfx, 111, 193, 10, 7, 255 );
			break;
		case 8:
			JD8_BlitCK( x, y, this->gfx, 121, 193, 10, 7, 255 );
			break;
		case 9:
			JD8_BlitCK( x, y, this->gfx, 131, 193, 10, 7, 255 );
			break;
	}
}
