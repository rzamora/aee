#pragma once

#include "info.h"

class ModuleSequence {

public:

	ModuleSequence( Info* info );
	~ModuleSequence(void);

	int Go();

private:

	void doIntro();
	void doMenu();
	void doSlides();
	void doBanner();
	void doSecreta();
	void doCredits();
    void doMort();

	Info* info;
	int contador;
};
