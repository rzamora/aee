#pragma once

#include "jdraw8.h"
#include "info.h"
#include "prota.h"

class Marcador {

public:

	Marcador( JD8_Surface gfx, Info* info, Prota* sam );
	~Marcador(void);

	void draw();

protected:

	void pintaNumero( Uint16 x, Uint16 y, Uint8 num );

	JD8_Surface	gfx;
	Info*		info;
	Prota*		sam;

};
