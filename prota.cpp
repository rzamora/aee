#include "prota.h"
#include "jgame.h"
#include "jinput.h"

Prota::Prota( JD8_Surface gfx, Info* info ) : Sprite( gfx ) {
	this->info = info;

	this->entitat = (Entitat*)malloc( sizeof( Entitat ) );
	this->entitat->num_frames = 82;
	this->entitat->frames = (Frame*)malloc( this->entitat->num_frames * sizeof( Frame ) );
	Uint16 frame = 0;
	for( int y = 0; y < 4; y++ ) {
		for( int x = 0; x < 5; x++ ) {
			this->entitat->frames[frame].w = 15;
			this->entitat->frames[frame].h = 15;
			if( this->info->num_piramide == 4 ) this->entitat->frames[frame].h -= 5;
			this->entitat->frames[frame].x = x*15;
			this->entitat->frames[frame].y = 20+(y*15);
			frame++;

		}
	}
	for( int y = 95; y < 185; y+=30 ) {
		for( int x = 60; x < 315; x+=15 ) {
			if( x != 300 || y != 155 ) {
				this->entitat->frames[frame].w = 15;
				this->entitat->frames[frame].h = 30;
				if( this->info->num_piramide == 4 ) this->entitat->frames[frame].h -= 5;
				this->entitat->frames[frame].x = x;
				this->entitat->frames[frame].y = y;
				frame++;
			}
		}
	}
	for( int y = 20; y < 50; y+=15 ) {
		for( int x = 225; x < 315; x+=15 ) {
			this->entitat->frames[frame].w = 15;
			this->entitat->frames[frame].h = 15;
			if( this->info->num_piramide == 4 ) this->entitat->frames[frame].h -= 5;
			this->entitat->frames[frame].x = x;
			this->entitat->frames[frame].y = y;
			frame++;
		}
	}

	this->entitat->num_animacions = 6;
	this->entitat->animacions = (Animacio*)malloc( this->entitat->num_animacions * sizeof( Animacio ) );
	for( int i = 0; i < 4; i++ ) {
		this->entitat->animacions[i].num_frames = 8;
		this->entitat->animacions[i].frames = (Uint8*)malloc( 8 );
		this->entitat->animacions[i].frames[0] = 0 + (i*5);
		this->entitat->animacions[i].frames[1] = 1 + (i*5);
		this->entitat->animacions[i].frames[2] = 2 + (i*5);
		this->entitat->animacions[i].frames[3] = 1 + (i*5);
		this->entitat->animacions[i].frames[4] = 0 + (i*5);
		this->entitat->animacions[i].frames[5] = 3 + (i*5);
		this->entitat->animacions[i].frames[6] = 4 + (i*5);
		this->entitat->animacions[i].frames[7] = 3 + (i*5);
	}
	this->entitat->animacions[4].num_frames = 50;
	this->entitat->animacions[4].frames = (Uint8*)malloc( 50 );
	for( int i = 0; i < 50; i++ ) this->entitat->animacions[4].frames[i] = i + 20;

	this->entitat->animacions[5].num_frames = 48;
	this->entitat->animacions[5].frames = (Uint8*)malloc( 48 );
	for( int i = 0; i < 12; i++ ) this->entitat->animacions[5].frames[i] = i + 70;
	for (int i = 12; i < 48; i++) this->entitat->animacions[5].frames[i] = 81;

	this->cur_frame = 0;
	this->x = 150;
	this->y = 30;
	this->o = 0;
	this->cycles_per_frame = 4;
	this->pergami = false;
	this->frame_pejades = 0;
}

void Prota::draw() {

	Sprite::draw();

	if( this->info->num_piramide == 4 && this->o != 4) {
		if( ( JG_GetCycleCounter() % 40 ) < 20 ) {
			JD8_BlitCK(this->x, this->y, this->gfx, 220, 80, 15, 15, 255 );
		} else {
			JD8_BlitCK(this->x, this->y, this->gfx, 235, 80, 15, 15, 255 );
		}
	}
}

Uint8 Prota::update() {

	Uint8 eixir = 0;

	if( this->o < 4 ) {
		Uint8 dir = 0;
		if ( JI_KeyPressed(SDL_SCANCODE_DOWN) ) {
			if( (this->x-20)%65 == 0 ) {
				this->o = 0;
				dir = 1;
			} else {
				dir = this->o + 1;
			}
		}
		if ( JI_KeyPressed(SDL_SCANCODE_UP) ) {
			if( (this->x-20)%65 == 0 ) {
				this->o = 1;
				dir = 2;
			} else {
				dir = this->o + 1;
			}
		}
		if( JI_KeyPressed( SDL_SCANCODE_RIGHT ) ) {
			if( (this->y-30)%35 == 0 ) {
				this->o = 2;
				dir = 3;
			} else {
				dir = this->o + 1;
			}
		}
		if( JI_KeyPressed( SDL_SCANCODE_LEFT ) ) {
			if( (this->y-30)%35 == 0 ) {
				this->o = 3;
				dir = 4;
			} else {
				dir = this->o + 1;
			}
		}
		switch( dir ) {
			case 1:
				if( this->y < 170 ) this->y++;
				break;
			case 2:
				if( this->y > 30 ) this->y--;
				break;
			case 3:
				if( this->x < 280 ) this->x++;
				break;
			case 4:
				if( this->x > 20 ) this->x--;
				break;
		}
		if( dir == 0 ) {
			this->cur_frame = 0;
		} else {
			this->frame_pejades++;
			if( this->frame_pejades == 15 ) this->frame_pejades = 0;
			if( JG_GetCycleCounter() % this->cycles_per_frame == 0 ) {
				this->cur_frame++;
				if( this->cur_frame == this->entitat->animacions[this->o].num_frames ) this->cur_frame = 0;
			}
		}
		eixir = false;
	} else {
		if( JG_GetCycleCounter() % this->cycles_per_frame == 0 ) {
			this->cur_frame++;
			if( this->cur_frame == this->entitat->animacions[this->o].num_frames ) {
				if( this->o == 4 ) { eixir = 1; } else { eixir = 2; }
			}
		}
	}
	return eixir;
}

