#pragma once

struct Info {
	int num_piramide;
	int num_habitacio;
	int diners;
	int diamants;
	int vida;
	int momies;
	int engendros;
	bool nou_personatge;
	bool pepe_activat;
};
