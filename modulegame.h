#pragma once

#include "info.h"
#include "mapa.h"
#include "prota.h"
#include "marcador.h"
#include "momia.h"
#include "bola.h"

class ModuleGame {

public:

	ModuleGame( Info* info );
	~ModuleGame(void);

	int Go();

private:

	void Draw();
	void Update();

	void iniciarMomies();

	Uint8 final;
	Info* info;
	JD8_Surface gfx;

	Mapa*		mapa;
	Prota*		sam;
	Marcador*	marcador;
	Momia*		momies;
	Bola*		bola;

};
