#pragma once

#include "jdraw8.h"

struct Frame {
	Uint16 x;
	Uint16 y;
	Uint16 w;
	Uint16 h;
};

struct Animacio {
	Uint8		num_frames;
	Uint8*		frames;
};

struct Entitat {
	Uint8		num_frames;
	Frame*		frames;
	Uint8		num_animacions;
	Animacio*	animacions;
};

class Sprite {

public:

	Sprite( JD8_Surface gfx );
	~Sprite(void);

	void draw();

	Entitat*	entitat;
	Uint8		cur_frame;
	Uint16		x;
	Uint16		y;
	Uint16		o;

protected:

	JD8_Surface gfx;
	Uint8		cycles_per_frame;

};
